# ant-exercises-sklearn

#### 介绍
scikit-learn 编程练习 100例

#### 课程简介：
- 提高您的机器学习技能并解决 100 多个 Python、numpy、pandas 和 scikit-learn 练习！
- 本课程专为具有 Python、numpy、pandas和scikit-learn基础知识的人设计。
- 它包含100 个带有解决方案的练习。
- 对于正在学习机器学习并正在寻找新挑战的人来说，这是一个很好的测试。
- 练习也是面试前的一个很好的测试。
- 本课程涵盖了许多热门话题。

#### 本课程适用于：
- 每个想边做边学的人
- 每个想要提高 Python 编程技能的人
- 每个想要提高数据科学技能的人
- 每个想要提高机器学习技能的人
- 每个想要准备面试的人

#### 课程包含19章内容：
- 缺失值处理
- 数值离散化
- 特征提取
- IRIS数据集
- 类别字段编码
- 乳腺癌数据集
- 线性回归
- 多项式特征
- 数值标准化
- 数据指标计算
- 决策树
- 随机森林
- 文本数据处理
- 数据聚类
- 数据降维PCA
- 关联规则
- 异常值检测
- 手写数字识别
- 邮件文本分类
- 房价预估

#### 覆盖sklearn知识点：
- 为机器学习模型准备数据
- 处理缺失值，SimpleImputer类
- 分类、回归、聚类
- 离散化
- 特征提取
- PolynomialFeatures类
- LabelEncoder类
- OneHotEncoder类
- StandardScaler类
- 虚拟编码
- 将数据拆分为训练集和测试集
- LogisticRegression类
- 混淆矩阵
- 分类报告
- 线性回归类
- MAE - 平均绝对误差
- MSE - 均方误差
- sigmoid()函数
- 熵
- 准确率
- 决策树分类器类
- GridSearchCV类
- RandomForestClassifier类
- CountVectorizer类
- TfidfVectorizer类
- KMeans类
- AgglomerativeClustering类
- 层次聚类类
- DBSCAN类
- 降维，PCA 分析
- 关联规则
- LocalOutlierFactor类
- 隔离森林类
- KNeighborsClassifier类
- 多项式NB 类
- GradientBoostingRegressor类

#### 联系方式
- 老师微信：ant_learn_python
- 微信公众号：蚂蚁学Python
